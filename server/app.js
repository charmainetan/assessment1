/**SERVER SIDE CODE */

console.log("starting...");

var express = require("express");
var bodyParser = require("body-parser");

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

console.log(__dirname);
console.log(__dirname + "/../client/");
const NODE_PORT = process.env.NODE_PORT || 3000;

app.use(express.static(__dirname + "/../client/"));



app.post("/users", function(req, res) {
    console.log("Received user object " + JSON.stringify(req.body));
    var user = req.body;
    user.email = "hi " + user.email;
    console.log("email > " + user.email);
    console.log("password > " + user.password);
    res.status(200).json(user);
});



app.listen(NODE_PORT, function() {
    console.log("Web App started at " + NODE_PORT);
});