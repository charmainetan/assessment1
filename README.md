##Prepare project structure (in Terminal)
1. Create a new working folder ```mkdir <project name>```
1. Create a sub-folder for server stuff ```mkdir <project name>/server```
1.Create a sub-folder for client files such as html & css pages `mkdir <project name>/client`

##Setup Bitbucket
1. Create new repo on bitbucket
1. Choose public or private access level

##Prepare Git
1. Initialize directory as a repository `git init`
1. Create an ignore-list for certain node_modules that should not appear on remote director `.gitignore`
1. Clone if necessary: `
1. Check which remote directory is the current directory configured to `git remote -v`
1. Remove remote URL from repository: `git remote remove origin`
1. Point the local repo to the remote repo: `git remote add origin <http git url>`
1. Add files to staging area: `git add . ` / `git add filename.type`
1. Commit changes to local repo: `git commit -m "comment"`
1. Push changes to remote repo: `git push -u origin master`
1. Check if branch is up-to-date with origin/master: `git status`

##Global Utility Installation
1. Use NPM to install Nodemon: `npm install -g nodemon`
1. Use NPM to install Bower: `npm install -g bower`

##Prepare Express JS
1. Initialise NPM in project directory: `npm init`
1. Install Express: `npm install express --save` web application middleware 

##Install Body-parser
1. Install Body-parser: `npm install body-parser --save` 

##Server-side Setup of Express
1. var app = express(); //initialise express
1. app.use(bodyParser.urlencoded({extended:false})); //tells the system whether you want to use simple (false) or complex (true) algorithm for parsing
1. app.use(bodyParser.json()); //instruct express interpret messages from browser as json

##How to start my app
1. Set a port number: `export NODE_PORT=3000` //operating system environment variable
1. Run nodemon to check: `nodemon or nodemon server/app.js`
1. Open browser to check app on at `localhost:3000`

##Bower
1. Create .bowerrc file in project directory. Add the following lines in .bowerrc file: `{"directory": "client/bower_components"}`
1. Initialise bower in terminal: `bower init`
1. Install Angular: `bower install angular --save`
1. Install Bootstrap: `bower install bootstrap --save`
1. Install FontAwesome: `bower install --save fontawesome`

Add to app.js:
```app.use("/bower_components", express.static(__dirname + "/../client/bower_components/"));```

Attach controller in app.js and reference in index.html
<html lang="en" ng-app="RegApp">
<div ng-controller="RegistrationCtrl as ctrl" ....

bower install --save angular-animate


###Other Notes
NPM and Bower are both dependency management tools. The main difference between both is NPM is used for installing Node js modules but Bower js is used for managing front end components like html, css, js etc. 
Handles dependencies to remove incompatibility issues (e.g. between jquery and bootstrap versions, etc.)

bodyParser,registering module/submodule/plugin to express. enabling the body to take in json form.
`npm install body-parser --save`
post "localhost:3000/user"
body--> raw--> JSON
type and send some user details:
{
    "user": {
        "name": "Kenneth",
        "age": 35
        }
}

app.post("/users", function(req,res){
    console.log("Received user object " + req.body);
    console.log("Received user object " + JSON.stringify(req.body));
    
    res.status(200).json(req.body.user); // must put here or else it's a bug.
});
